//
//  MovieCollectionViewCell.swift
//  AMC_CodeInterview
//
//  Created by Sarah on 9/28/18.
//  Copyright © 2018 toltoly. All rights reserved.
//

import UIKit

class MovieCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var movieImage: UIImageView!
    
    func setMovie(movie:Movie){
        movieImage.image=movie.image

    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = 8.0
        self.clipsToBounds=true
    }
}
