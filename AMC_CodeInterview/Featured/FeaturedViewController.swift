//
//  FeaturedViewController.swift
//  AMC_CodeInterview
//
//  Created by Sarah on 9/28/18.
//  Copyright © 2018 toltoly. All rights reserved.
//

import UIKit

class FeaturedViewController: UIViewController {

    @IBOutlet weak var carouselView: CarouselView!
    
    @IBOutlet weak var tableView: UITableView!
    
    var genres:[String]=["Newly Added","Curator's Choice","Best Soundtracks","Get Rad","Vengences is Here","Black Magic","Haunts"]
    var movies:[Movie] = []
    var moviesInCarousel:[Movie] = []
    var collectionViewCellOffsets = [Int:CGFloat]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self

        movies = createMovieArray()
        moviesInCarousel = createMovieInCarouselArray()
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        
        carouselView.setCarousel(movies: moviesInCarousel)
    }
    
    func createMovieInCarouselArray()->[Movie]{
        
        //original item [A,B,C]
        //Extend item to  [C,A,B,C,A] for infinite scrolling
        var tempMovie:[Movie]=[]
        let movie1 = Movie(image:#imageLiteral(resourceName: "poster3"), title: "Forrest Gump") //C
        let movie2 = Movie(image:#imageLiteral(resourceName: "poster"), title: "Back to the future") //A
        let movie3 = Movie(image:#imageLiteral(resourceName: "poster2"), title: "Indiana Jones") //B
        let movie4 = Movie(image:#imageLiteral(resourceName: "poster3"), title: "Forrest Gump") //C
        let movie5 = Movie(image:#imageLiteral(resourceName: "poster"), title: "Back to the future") //A

   
        
        tempMovie.append(movie1)
        tempMovie.append(movie2)
        tempMovie.append(movie3)
        tempMovie.append(movie4)
        tempMovie.append(movie5)

        
        return tempMovie
    }
    
    func createMovieArray()->[Movie]{
        
        var tempMovie:[Movie]=[]
        let movie1 = Movie(image:#imageLiteral(resourceName: "poster"), title: "Back to the future")
        let movie2 = Movie(image:#imageLiteral(resourceName: "poster2"), title: "Indiana Jones")
        let movie3 = Movie(image:#imageLiteral(resourceName: "poster3"), title: "Forrest Gump")
        let movie4 = Movie(image:#imageLiteral(resourceName: "poster"), title: "Back to the future")
        let movie5 = Movie(image:#imageLiteral(resourceName: "poster2"), title: "Indiana Jones")
        let movie6 = Movie(image:#imageLiteral(resourceName: "poster3"), title: "Forrest Gump")
        let movie7 = Movie(image:#imageLiteral(resourceName: "poster"), title: "Back to the future")
        let movie8 = Movie(image:#imageLiteral(resourceName: "poster2"), title: "Indiana Jones")
        let movie9 = Movie(image:#imageLiteral(resourceName: "poster3"), title: "Forrest Gump")
        
        tempMovie.append(movie1)
        tempMovie.append(movie2)
        tempMovie.append(movie3)
        tempMovie.append(movie4)
        tempMovie.append(movie5)
        tempMovie.append(movie6)
        tempMovie.append(movie7)
        tempMovie.append(movie8)
        tempMovie.append(movie9)
        
        return tempMovie
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension FeaturedViewController: UITableViewDataSource,UITableViewDelegate{
    
  
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return movies.count
        return genres.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        

        let cell = tableView.dequeueReusableCell(withIdentifier: "FeaturedTableCell") as! FeaturedTableViewCell
        cell.genreLabel.text=genres[indexPath.row]
        return cell
        
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard let tableViewCell = cell as? FeaturedTableViewCell else {
            return;
        }
        tableViewCell.setCollectionViewDelegate(dataSourceDelegate: self, forRow: indexPath.row)
        tableViewCell.collectionViewOffset = collectionViewCellOffsets[indexPath.row] ?? 0
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard let tableViewCell = cell as? FeaturedTableViewCell else { return }
        
        //Cache current x position of CollectionViewCell
        collectionViewCellOffsets[indexPath.row] = tableViewCell.collectionViewOffset
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 145
    }
  
}

extension FeaturedViewController: UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let alert = UIAlertController(title: "SHUDDER", message: movies[indexPath.row].title, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        
        return movies.count;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionCell", for: indexPath) as! MovieCollectionViewCell
        cell.setMovie(movie: movies[indexPath.row])
        return cell
    }
}








