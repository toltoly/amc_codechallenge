//
//  FeaturedTableViewCell.swift
//  AMC_CodeInterview
//
//  Created by Sarah on 9/28/18.
//  Copyright © 2018 toltoly. All rights reserved.
//

import UIKit

class FeaturedTableViewCell: UITableViewCell {
    @IBOutlet weak var genreLabel: UILabel!
    
    @IBOutlet weak var collectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }



    

}
extension FeaturedTableViewCell
{
    func setCollectionViewDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(dataSourceDelegate: D, forRow row: Int) {
    
        collectionView.delegate = dataSourceDelegate
        collectionView.dataSource = dataSourceDelegate
        collectionView.tag = row
        collectionView.setContentOffset(collectionView.contentOffset, animated:false) // Stops collection view if it was scrolling.
        collectionView.reloadData()
        
    }
    
    var collectionViewOffset:CGFloat{
        set{
            collectionView.contentOffset.x = newValue
            
        }
        get{
            return collectionView.contentOffset.x
            
        }
    }
}
