//
//  CarouselView.swift
//  AMC_CodeInterview
//
//  Created by Sarah on 9/29/18.
//  Copyright © 2018 toltoly. All rights reserved.
//

import UIKit

class CarouselView: UICollectionView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    var scrollDirectionDetermined:Bool = false
    var centerCellIndex:CGFloat = 1.0
    
    let scaleMinimum: CGFloat = 0.8
    
    var movies:[Movie] = []

    var cellWidthIncludeSpacing :CGFloat = 0.0
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.dataSource=self
        self.delegate=self

        let layout = self.collectionViewLayout as! UICollectionViewFlowLayout
        cellWidthIncludeSpacing = layout.itemSize.width + layout.minimumLineSpacing
        
        let insetX = (self.bounds.width - layout.itemSize.width) / 2.0
        let insetY = (self.bounds.height - layout.itemSize.height) / 2.0
    
        self.contentInset = UIEdgeInsets(top: insetY, left: insetX, bottom: insetY, right: insetX)

    }
    

    func setCarousel(movies:[Movie])
    {
        self.movies = movies

        updateContentOffset(index:centerCellIndex)
        self.setNeedsLayout()
        self.layoutIfNeeded()
    }

}

extension CarouselView:UICollectionViewDelegate,UICollectionViewDataSource{
    

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let alert = UIAlertController(title: "SHUDDER", message: movies[indexPath.row].title, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert

        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        
        return self.movies.count;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CarouselCell", for: indexPath) as! CarouselViewCell
        
       // NSLog("CenterIndex %f", centerCellIndex)
        if indexPath.row == Int(centerCellIndex){
            cell.scale=1;
        }
        else
        {
            cell.scale=scaleMinimum
        }
        cell.index = indexPath.row
        cell.setMovie(movie: movies[indexPath.row])
        DispatchQueue.main.async {
            cell.setNeedsLayout()
            cell.layoutIfNeeded()
        }
        return cell
    }
    

    
}

extension CarouselView:UIScrollViewDelegate{


    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {


        if velocity.x > 0
        {
            if centerCellIndex < CGFloat(self.movies.count - 1)
            {
                centerCellIndex += 1
                if CGFloat(self.movies.count - 1) == centerCellIndex
                {
                    centerCellIndex = 1
                }
                
            }
            
        }
        else
        {
            if centerCellIndex > 0
            {
                centerCellIndex -= 1
                if 0.0 == centerCellIndex
                {
                    centerCellIndex = CGFloat(self.movies.count - 2)
                }
            }
        }

        let offset = CGPoint(x: centerCellIndex * cellWidthIncludeSpacing - scrollView.contentInset.left, y: -scrollView.contentInset.top)
        targetContentOffset.pointee = offset

        for cell in self.visibleCells  as! [CarouselViewCell]   {

            if cell.index == Int(centerCellIndex){
                cell.scale=1;
            }
            else
            {
                cell.scale=scaleMinimum
            }
            DispatchQueue.main.async {
                cell.setNeedsLayout()
                cell.layoutIfNeeded()
            }

        }
    }

    func updateContentOffset(index:CGFloat)
    {
        let offset = CGPoint(x: index * cellWidthIncludeSpacing - self.contentInset.left, y: -self.contentInset.top)
        self.contentOffset=offset
    }

}
