//
//  CarouselViewCell.swift
//  AMC_CodeInterview
//
//  Created by Sarah on 9/30/18.
//  Copyright © 2018 toltoly. All rights reserved.
//

import UIKit

class CarouselViewCell: UICollectionViewCell {
    
    
    var index:Int = 0
    
    var scale:CGFloat = 1.0 {

        didSet{

            movieImage.transform = CGAffineTransform.identity.scaledBy(x: scale, y: scale)
        }
    }
    @IBOutlet weak var movieImage: UIImageView!
    
    func setMovie(movie:Movie){
        movieImage.image=movie.image
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = 8.0
        self.clipsToBounds=true

    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        movieImage.transform = CGAffineTransform.identity
    }

}
