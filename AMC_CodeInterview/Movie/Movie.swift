//
//  Movie.swift
//  AMC_CodeInterview
//
//  Created by Sarah on 9/28/18.
//  Copyright © 2018 toltoly. All rights reserved.
//

import Foundation
import UIKit

class Movie{
    

    var image:UIImage
    var title:String
    
    init(image:UIImage,title:String) {
        
        self.image=image
        self.title=title
    }

}
